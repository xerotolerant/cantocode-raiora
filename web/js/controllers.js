raioraApp.controller('MainCtrl', function($scope, kandyChat, $timeout, $window, $location){
  /*
  var forceSSL = function () {
    if ($location.protocol() !== 'https') {
        $window.location.href = $location.absUrl().replace('http', 'https');
    }
  };
  forceSSL();
  */

})

raioraApp.controller('MapCtrl', function($scope, kandyChat){
  var map;
  $scope.message = "Map Success!";
  initMap();
  function initMap() {
    console.log("Init Map stated");
    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 10.5827821, lng: -61.3079332},
      scrollwheel: false,
      zoom: 10
    });
    var listener1 = map.addListener('idle', function(){
      var marker1 = new google.maps.Marker({
        position: {lat: 10.580521874461846, lng: -61.342620853101835},
        map: map,
        animation: google.maps.Animation.DROP,
        label: "flood"
      })
      var marker2 = new google.maps.Marker({
        position: {lat: 10.44954965738448, lng: -61.425018324516714},
        map: map,
        animation: google.maps.Animation.DROP
      })
      var marker3 = new google.maps.Marker({
        position: {lat: 10.303659953925152 , lng: -61.30279542412609},
        map: map,
        animation: google.maps.Animation.DROP
      })
      var marker4 = new google.maps.Marker({
        position: {lat: 10.534620419474482 , lng: -61.41815186943859},
        map: map,
        animation: google.maps.Animation.DROP
      })
      var marker5 = new google.maps.Marker({
        position: {lat: 10.287445799531902 , lng: -61.44287110771984},
        map: map,
        animation: google.maps.Animation.DROP
      })
      google.maps.event.removeListener(listener1);
    })
    map.addListener('click', function(event){
      console.log(event.latLng.lat(), event.latLng.lng());
      var marker = new google.maps.Marker({
        latLng: event.latLng,
        map: map,
        position: event.latLng,
        animation: google.maps.Animation.DROP,

      })

    })
  }

  $scope.sendDistressSignal = function(currentLocation){
    //get currentLocation
    navigator.geolocation.getCurrentPosition(
      function(pos){
        console.log(pos);

        //sendMessage to Emergency service
        kandy.messaging.sendJSON(
          'rescuer@raiora.hotmail.com',
          pos,
          function(){
            console.log("Message Sent. Help is on the way.")
            //drop emergencyMarker
            var emergencyMarker = new google.maps.Marker({
              position: {lat: pos.coords.latitude, lng: pos.coords.longitude},
              map: map,
              animation: google.maps.Animation.DROP
            })
          },
          function(){console.log("message failed to send.")}
        )
      },
      function(e){
        console.log(e.message);
      }
    )

  }
});

raioraApp.controller('ChatCtrl', function($scope, kandyChat, $timeout){
  kandy.setup({
    listeners:{
      message: onMessageReceived,
      chatGroupMessage: onChatGroupMessage,
      chatGroupInvite: onChatGroupInvite,
      chatGroupBoot: onChatGroupBoot,
      chatGroupUpdate: onChatGroupUpdate,
      chatGroupDelete: onChatGroupDelete
    }
  })

  //group stuff

//moved this to kandyChat service
  // Gather the user's input then creates a group.
  $scope.CreateGroup = function createGroup(groupName) {
    //  var name = document.getElementById("create-group-name").value;
      // Tell Kandy to create the group.
      kandy.messaging.createGroup(groupName, null, onCreateSuccess, onCreateFailure);
  }


$scope.createdGroups = [];
  //group create success
  function onCreateSuccess(group) {

    $timeout(function(){  $scope.createdGroups.push(group)}, 500);

    console.log("Group created: " + group.group_name + " (" + group.group_id + ").");
    console.log($scope.createdGroups);
    alert("Group: " + group.group_name + " Added");
}
//group create failure
function onCreateFailure() {
    console.log("Failed to create group.");
}


// Gather the user's input then delete the specified group.
$scope.deleteGroup = function deleteGroup(delGroupID) {
        // Tell Kandy to delete the group.
    kandy.messaging.deleteGroup(delGroupID, onDeleteSuccess, onDeleteFailure);
}

// What to do on a delete group success.
function onDeleteSuccess() {

    console.log("Group deleted.");
}

// What to do on a delete group failure.
function onDeleteFailure() {
    console.log("Failed to delete group.");
}


//group update success
function onUpdateSuccess(group) {
    console.log("Group updated: " + group.group_name + " (" + group.group_id + ").");
}
//group update failure
function onUpdateFailure() {
    console.log("Failed to update group.");
}
//add group member
$scope.addMember = function(groupID,new_groupuser_ID){
  var new_groupusers= new_groupuser_ID.split(", ");
 kandy.messaging.addGroupMembers(groupID,new_groupusers, onAddSuccess, onAddFailure);
}

//group member added successfully
function onAddSuccess(group) {
    console.log("Members added to group.");
}

//group member add failed
function onAddFailure() {
    console.log("Failed to add members to group.");
}


$scope.groupMessages = [];


$scope.sendGroupMessage = function(groupID,messageString){

kandy.messaging.sendGroupIm(groupID, messageString,
    function(s){


      var element = "<div> Me @ (" + groupID + "): " + messageString + "</div>";

         // Add the message to the chat messages div.
         document.getElementById("groupchat-messages").innerHTML += element;

      console.log("Group Message:" + messageString + " sent successfully to Group ID: " + groupID)},
    function(e){console.log("message sending failed")}
    )
}


//group messages received
function onChatGroupMessage(groupmessage) {


   // Create the message element. Use Lodash to escape the message to prevent security issues.
   var element = "<div>" + groupmessage.sender.user_id + " (" + groupmessage.group_id + "): " + groupmessage.message.text + "</div>";

   // Add the message to the chat messages div.
   document.getElementById("groupchat-messages").innerHTML += element;

}

// Called when a user is added to a group.
function onChatGroupInvite(message) {
    var id = message.group_id;
    var inviter = message.inviter;
    var invitees = message.invitees.join(", ");

    $scope.inviteesMessage =  invitees + " was added to group " + id + " by " + inviter + ".";
}

// Called when a user is removed from a group.
function onChatGroupBoot(message) {
    var id = message.group_id;
    var booter = message.booter;
    var booted = message.booted.join(", ");

  $scope.removedFromGroupMessage = booted + " was removed from group " + id + " by " + booter + ".";
}

// Called when a group is updated.
function onChatGroupUpdate(message) {
    var id = message.group_id;
    var updater = message.updater;
    var name = message.group_name;



  $scope.groupUpdatedMessage = id + " was updated by " + updater + " to " + name + ".";
}

// Called when a group is deleted.
function onChatGroupDelete(message) {
    var id = message.group_id;
    var eraser = message.eraser;

    $scope.deletegroupmessage =  "Group " +   id + " was deleted by " + eraser + ".";

}

//end of group stuff

  function onMessageReceived(message){
    console.log("message received");
    console.log(message);


    var element = "<div>Incoming << (" +message.sender.user_id + "): " + (message.message.text) + "</div>"
   document.getElementById("chat-messages").innerHTML += element;

  }


  $scope.date = new Date();
  $scope.chatmessages = [];
  $scope.receivedMessages = [];

  $scope.sendMessage = function(messageRecipient,messageString){

    var msgRecipient = messageRecipient + "@raiora.hotmail.com";


    kandy.messaging.sendIm(msgRecipient, messageString,
      function(s){

        var msgItem = msgRecipient + "," + messageString;

        var element = "<div> Outgoing >>(" + msgRecipient + "): " + messageString + "</div>";
           document.getElementById("chat-messages").innerHTML += element;


        console.log("Message sent successfully")},
      function(e){console.log("message sending failed")}
      )
  }

//accepts a comma separated list of callee numbers to send SMS messages to
$scope.sendSMS= function (calleeNumbers,callerNumber,messageString){
var calleeList = calleeNumbers.split(",");
for (var i = 0; i < calleeList.length; i++) {
  kandy.messaging.sendSMS(calleeList[i], callerNumber, messageString,
  function(s){
    console.log("Sms sent to:" +calleeList[i] + ">> " + messageString )
  },
  function(e){
    console.log ("SMS send error. No message delivered to "+calleeList[i])

  }
)
}
}



  $scope.message = "Chat Portal Loaded";

})

raioraApp.controller('AlertCtrl', function($scope){
  $scope.message = "Alert Success!";
})

raioraApp.controller('StatsCtrl', function($scope){
  $scope.message = "Stats Success!";
})
raioraApp.controller("LoginCtrl", function($scope, kandyChat, $timeout, $state){
  $scope.login = function(user){
    console.log(user);
    kandyChat.login(user);
    $scope.loggingIn = true;
    checkLogin(kandyChat.loggedIn);
  }
  console.log(kandyChat.loggedIn);

  function checkLogin(loggedIn){
    $scope.loginMessage = kandyChat.loginMessage();
    if (kandyChat.loginMessage() !== "") {
      $scope.loggingIn = false;
      return
    }

    if (kandyChat.loggedIn() == true){

      console.log("logged in!");
      $state.go('dash.home');
    } else {

      console.log("loggin in");
      $timeout(function(){checkLogin()}, 500);
    }
  }
})

raioraApp.controller('CallsCtrl', function($scope, kandyChat, $timeout, $state){
  var remoteContainer = document.getElementById("remoteContainer");
  var localContainer = document.getElementById("localContainer");
  console.log(remoteContainer, localContainer);
  var kandyOptions = {
    remoteContainer: document.getElementById("remoteContainer"),
    localContainer: document.getElementById("localContainer")
  }
  kandy.call.initMedia(
    function(){console.log('media Initialized')},
    function(){console.log("media initialization failed")
  })
  kandy.setup({
    remoteVideoContainer: document.getElementById("remoteContainer"),
    localVideoContainer: document.getElementById("localContainer"),
    listeners: {
        media: onMediaError,
        callinitiated: onCallInitiated,
        callincoming: onCallIncoming,
        callestablished: onCallEstablished,
        callended: onCallEnded,

    }
  })//kandy.setup
    function onMediaError(e){
      console.log("Media error", e);
    }
    function onCallInitiated(){
      console.log("Call initiated")
    }
    $scope.callId = null;
    function onCallIncoming(incomingCall){
      console.log('Call Incoming...', incomingCall.getId());

      $timeout(function(){$scope.callId = incomingCall.getId()}, 500)

    }
    function onCallEstablished(){
      console.log('callEstablished')
    }
    function onCallEnded(){
      console.log('Call Ended');
    }


  //call to a user via their ID
    $scope.makeCall = function(callee){
      var showVideo = true;
      console.log("starting call...");
      kandy.call.makeCall(callee + "@raiora.hotmail.com", showVideo);
    }
  //call a user via their phone number
  $scope.makePSTNCall = function (calleeNumber,callerID){

  kandy.call.makePSTNCall(calleeNumber, callerID);

  }
  $scope.acceptCall = function(callId){

      console.log("answering Call")


      kandy.call.answerCall(callId, true);

  }
  $scope.declineCall = function(callId){
      console.log("rejecting call...")
      kandy.call.rejectCall(callId);
  }
  $scope.endCall = function(callId){
      console.log("ending call");
      kandy.call.endCall(callId);
  }
})
raioraApp.controller('DashCtrl', function($scope, $timeout, kandyChat){

  $scope.message = "Building a safer world together.";
  $scope.addingContact = false;
  $scope.newContact = function(){
    $scope.addingContact = !$scope.addingContact;
  }
  getAddressBook();
  function getAddressBook() {
    kandy.addressbook.retrievePersonalAddressBook(onGetSuccess, onGetFailure);
  }

  // Call Kandy's function to get the address book.
  $scope.getAddressBook = function getAddressBook() {
    console.log("getting address book");
    kandy.addressbook.retrievePersonalAddressBook(onGetSuccess, onGetFailure);
  }

  // What to do on a failed get address book.
  function onGetFailure() {
    console.log("Get Address Book failure.");
  }

  // What to do on a successful get address book.
  function onGetSuccess(contacts) {
    console.log("Get Address Book success.");
    // Clear the contact list of any existing contacts.
    //document.getElementById("list-group").innerHTML = "Contacts:";
    $scope.contacts = contacts;

  }
$scope.userGroups = [];
$scope.getGroups = function getGroups() {
      // Tell Kandy to get groups.
      kandy.messaging.getGroups(onGetGroupsSuccess, onGetGroupsFailure);
  }

  function onGetGroupsSuccess(result) {
      // Get the element we'll be displaying groups in.
  $timeout(function(){  $scope.userGroups.push(group)}, 500);
  }

  function onGetGroupsFailure() {
    log("Failed to get groups.");
}

  $scope.chatmessages = [];
  $scope.receivedMessages = [];

  $scope.sendMessage = function(messageRecipient,messageString){

  var msgRecipient = messageRecipient + "@raiora.hotmail.com";


  kandy.messaging.sendIm(msgRecipient, messageString,
    function(s){


      console.log("Message sent successfully")},
    function(e){console.log("message sending failed")}
    )
  }



  $scope.sendGroupMessage = function(groupID,messageString){

  kandy.messaging.sendGroupIm(groupID, messageString,
    function(s){
      var element = "<div> Me @ (" + groupID + "): " + messageString + "</div>";

         // Add the message to the chat messages div.
         document.getElementById("groupchat-messages").innerHTML += element;

      console.log("Group Message:" + messageString + " sent successfully to Group ID: " + groupID)},
    function(e){console.log("message sending failed")}
    )
  }


  //group messages received
  function onChatGroupMessage(groupmessage) {


   // Create the message element. Use Lodash to escape the message to prevent security issues.
   var element = "<div>" + groupmessage.sender.user_id + " (" + groupmessage.group_id + ")>> " + groupmessage.message.text + "</div>";

   // Add the message to the chat messages div.
   document.getElementById("groupchat-messages").innerHTML += element;

  }

  $scope.addContact = function addContact() {
    // Create a new contact object with the user's input.
    var contact = {
        "username": document.getElementById('username').value,
        "firstName": document.getElementById('firstName').value,
        "lastName": document.getElementById('lastName').value,
        "phonenumber": document.getElementById('phonenumber').value
    };

    // Extra check to make sure username isn't empty.
    if (typeof contact.username == "") {
        // Consider it a failed add contact if it was blank.
        onAddFailure("Please enter a username.");
        return;
    }

    // Call Kandy's function to add the contact.
    kandy.addressbook.addToPersonalAddressBook(contact, onAddSuccess, onAddFailure);
  }

  // Utility function for appending messages to the message div.
  function log(message) {
    document.getElementById("messages").innerHTML += "<div>" + message + "</div>";
  }

  // What to do on a successful add.
  function onAddSuccess() {
    kandy.addressbook.retrievePersonalAddressBook(onGetSuccess, onGetFailure);
    console.log("Add Contact success.");
  }

  // What to do on a failed add.
  function onAddFailure(message) {
    console.log("Add Contact failure. " + message);
  }
})
