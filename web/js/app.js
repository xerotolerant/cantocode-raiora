var raioraApp = angular.module('raioraApp',  ['ui.router']);

raioraApp.config(function($stateProvider, $urlRouterProvider){
  $urlRouterProvider.otherwise('login');
  $stateProvider
    .state('dash', {
      url: '/dash',
      templateUrl: '/templates/dash.html'
    })
    .state('dash.home', {
      url: '/home',
      templateUrl: 'pages/home.html',
      controller: 'DashCtrl'

    })
    .state('dash.chats', {
      url: '/chats',
      templateUrl: "pages/chat.html",
      controller: 'ChatCtrl'
    })
    .state('dash.calls', {
      url: '/calls',
      templateUrl: "pages/calls.html",
      controller: 'CallsCtrl'

    })
    .state('dash.stats', {
      url: "/stats",
      templateUrl: '/pages/map.html',
      controller: 'MapCtrl'
    })
    .state('login', {
      url: '/login',
      templateUrl: "login.html",
      controller: "LoginCtrl"
    })
})
